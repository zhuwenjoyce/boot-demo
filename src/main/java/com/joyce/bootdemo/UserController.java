package com.joyce.bootdemo;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class UserController {
    private static final org.slf4j.Logger LOG = org.slf4j.LoggerFactory.getLogger(UserController.class);

    @RequestMapping(value = "/user-init")
    @ResponseBody
    public Model user_init(Model m){
        m.addAttribute("myname","ZhangSan");
        return m;
    }

    @RequestMapping("/hello")
    public ModelAndView sayHello() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("hello");
        modelAndView.addObject("key", 13);
        LOG.info("请求进入/hi");
        return modelAndView;
    }

    @RequestMapping(value = "/admin-user")
    public String admin_user(Model model) {
        model.addAttribute("name", "dddabc");
        LOG.info("请求进入admin-user");
        return "admin";
    }
}
