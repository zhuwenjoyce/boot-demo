package com.joyce.bootdemo;

import com.alibaba.fastjson.JSONObject;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
public class JsonController {
	private static final org.slf4j.Logger LOG = org.slf4j.LoggerFactory.getLogger(JsonController.class);

	/**
	 * 成功
	 * @return
	 */
	@RequestMapping("/json-ok-1")
	public Object jsonOK1() {
		Map<String, Object> map2 = new HashMap<String, Object>();
		map2.put("say /json-ok-1", "我好菜!!!");
		LOG.info("请求进入/json-ok-1");
		return map2;
	}

	/**
	 * 成功
	 * @return
	 */
	@RequestMapping("/json-ok-2")
	public Object jsonOK2() {
		ModelMap map = new ModelMap();
		map.put("say json-ok-2", "我好菜啊啊啊!!!");
		LOG.info("请求进入/json-ok-2");
		return map;
	}

	/**
	 * 成功
	 * @return
	 */
	@RequestMapping("/json-ok-3")
	public Object jsonOk3(User user) {
		ModelMap map = new ModelMap();
		map.put("say /json-ok-3", "我好菜啊啊啊!!!");
		LOG.info("请求进入/json-ok-3");
		return map;
	}

	/**
	 * 成功
	 * @return
	 */
	@RequestMapping("/json-ok-4")
	public Object jsonOK4() {
		ModelMap map = new ModelMap();
		map.put("say /json-ok-4", "我好菜啊啊啊!!!");
		LOG.info("请求进入/json-ok-4");
		return map;
	}

	/**
	 * 成功
	 * @return
	 */
	@RequestMapping("/json-ok-5")
	public Object jsonOK5(String u,String p) {
		ModelMap map = new ModelMap();
		map.put("say /json-ok-5", "u==="+u+";p==="+p);
		LOG.info("请求进入/json-ok-5");
		return map;
	}

	/**
	 * 成功
	 * @return
	 */
	@RequestMapping("/json-ok-6")
	public Object jsonOK6(User u) {
		ModelMap map = new ModelMap();
		map.put("say /json-ok-6", "u==="+ JSONObject.toJSONString(u));
		LOG.info("请求进入/json-ok-6;;"+ JSONObject.toJSONString(u));
		return map;
	}

	/**
	 * 成功
	 * http://localhost:8080/json-ok-7?id=888  返回：{"say /json-ok-7":"u1==={\"id\":\"888\"};;;u2=={\"id\":\"888\"}"}
	 *
	 * @return
	 */
	@RequestMapping("/json-ok-7")
	public Object jsonOK7(User u1,User u2) {
		ModelMap map = new ModelMap();
		String json1 = JSONObject.toJSONString(u1);
		String json2 = JSONObject.toJSONString(u2);
		map.put("say /json-ok-7", "u1==="+ json1+";;;u2=="+json2);
		LOG.info("请求进入/json-ok-7;;"+ "u1==="+ json1+";;;u2=="+json2);
		return map;
	}

	/**
	 * 成功
	 * http://localhost:8080/json-ok-8?id=888&b=bus 返回：{"say /json-ok-8":"u1==={\"id\":\"888\"};;;b=={\"b\":\"bus\"}"}
	 * @return
	 */
	@RequestMapping("/json-ok-8")
	public Object jsonOK8(User u,Bus b) {
		ModelMap map = new ModelMap();
		String json1 = JSONObject.toJSONString(u);
		String json2 = JSONObject.toJSONString(b);
		map.put("say /json-ok-8", "u1==="+ json1+";;;b=="+json2);
		LOG.info("请求进入/json-ok-8;;"+ "u1==="+ json1+";;;b=="+json2);
		return map;
	}

	/**
	 * 失败
	 * @param map
	 * @return
	 */
	@RequestMapping("/json1")
	public Object json1(ModelMap map) {
		map.put("say /json1", "100200");
		map.addAttribute("aaa","zzzz");
		LOG.info("请求进入/json1");
		return map;
	}

	/**
	 * 失败
	 * @param map2
	 * @return
	 */
	@RequestMapping("/json2")
	public Object json2(HashMap<String, Object> map2) {
		map2.put("say /json2", "我好菜啊啊啊!!!");
		LOG.info("请求进入/json2");
		return map2;
	}
}
